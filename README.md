# api-connector

## Getting started
This project contains the basics for uploading data in your iodash platform.

To make it easy for you to get started this repository will speed up your first upload.

- [ ] Create a virtual environment
- [ ] Install requirements
- [ ] Complete your .env file, see below.

- - [ ] Create a new client within the platform, copy CLIENT_ID and CLIENT_SECRET to .env variables.

```
CLIENT_ID = ""
CLIENT_SECRET = ""

AUTH_URL = https://portal.iodash.com/api/v1/auth
COLLECTION_URL = https://portal.iodash.com/api/v1/collections
EVENTS_URL = https://portal.iodash.com/api/v1/events
```

- [ ] Run iodash_api_connector.py

## Upload payload format
```
voorbeeld_data = {"collectie_naam": 
    [
        {"datetime": "2023-08-18T12:00:00", "string_waarde": "test", "int_waarde": 15, "float_waarde":  15.1},
        {"datetime": "2023-08-18T12:00:00", "string_waarde": "test2", "int_waarde": 16, "float_waarde":  15.3}

```
