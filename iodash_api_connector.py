from dotenv import dotenv_values
from datetime import datetime, timedelta

import requests

config = dict(dotenv_values(".env"))

print(config)

class API_Connector(object):
    def __init__(self) -> None:
        self.time_stamp = None
        self.expires_in = None
        self.access_token = None        
        
    def _generate_token(self):
        print(f"url: {config.get('AUTH_URL')}")

        try:
            data = {"grant_type": "client_credentials", "client_id": config.get('CLIENT_ID'), "client_secret": config.get('CLIENT_SECRET')}
            response = requests.post(config.get('AUTH_URL'), data=data)
            print(f"response: {response.json()}")
            print(f"Status code: {response.status_code}")

            if response.status_code == 200:
                response = response.json()

                self.access_token = response.get('access_token', None)
                self.expires_in = response.get('expires_in', None)
                self.time_stamp = datetime.now()

                print(f"expires_in: {self.expires_in} seconds / {int(self.expires_in) / 60 / 60 / 24} day(s), access_token: {self.access_token}")

                return True
        except Exception as e:            
            print(f"Could not generate token: {e}")
            return False
        
    def _validate_token(self):
        duration = datetime.now() - self.time_stamp if self.time_stamp else timedelta(0, 0)
        try:
            if (duration.total_seconds() > self.expires_in if self.expires_in else timedelta(0, 0)) or self.access_token == None:
                print(f"Token expired, {int(duration.total_seconds())} over due")
                
                if self._generate_token():
                    print(f"New token generated, {int(abs(self.expires_in - duration.total_seconds()))} seconds left") 

                    return True
            else:                
                print(f"Token still valid, {int(abs(self.expires_in - duration.total_seconds()))} seconds left")
                return True
            

        except Exception as e:            
            print(f"Token validation error: {e}")
            return False
        
    def post_events(self, data: dict):
        response = self._validate_token()

        print(f"data: {data}")

        if not response:
            return print(f"Token validation error")

        url = config.get('EVENTS_URL')

        bearer_header = {"Content-Type": "application/json", "Authorization": f"Bearer {self.access_token}"}

        try:
            response = requests.post(url, json=data, headers=bearer_header, timeout=20)
            
            if response.status_code == 201:
                return print(f"{response.text}")
            if response.status_code == 401:
                return print(f"{response.text}")
            if response.status_code == 500:
                return print(f"{response.text}")
            else:
                return print(f"{response.text}")
        except Exception as e:
            return print(f"Failed with error: {e}")
        
api_connector = API_Connector()

# voorbeeld data om in te sturen:
# collectie (data bucket) met vrij aan te maken velden moeten aan een volgend format voldoen:
voorbeeld_data = {"collectie_naam": 
    [
        {"datetime": "2023-08-18T12:00:00", "string_waarde": "test", "int_waarde": 15, "float_waarde":  15.1},
        {"datetime": "2023-08-18T12:00:00", "string_waarde": "test2", "int_waarde": 16, "float_waarde":  15.3}
    ]
}
api_connector.post_events(data=voorbeeld_data)